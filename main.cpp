#include <iostream>
using namespace std;

class Heap{
    int v[100],n;
    void pushUp(int i) {
        while(v[i] > v[i/2]) {  ///daca e mai mare decat tatal
            if(i==1) {  ///daca e radacina
                return;
            }
            swap(v[i], v[i/2]);     ///intersc nod cu tata
            i = i/2;    ///verifica si pt tata
        }
    }
    void pushDown(int i){   ///sch cu max dintre fii pana se respecta ordinea/ ajunge frunza
        int maxim = i;
        if(v[i] < v[2*i] && 2*i <= n) {    ///fiul stang e mai mare
            maxim = 2*i;
        }
        if(v[maxim] < v[2*i + 1] && (2*i +1 ) <= n) {    ///fiul drept e mai mare decat max
            maxim  = 2*i + 1;
        }
        if(maxim != i) {    ///daca max s-a schimbat
            swap(v[i], v[maxim]);
            pushDown(maxim);
        }
    }
public:
    Heap() {
        n=0;
    }
    Heap(int v2[], int n2) {
        this->n = n2;
        for(int i=1; i <= n; i++) {     ///copiez elem in v
            this->v[i] = v2[i];
        }
        ///aplic pushDown nodurilor care nu sunt frunze pt a avea prop de HEAP
        for(int i = n/2; i >= 1; i--) {
            pushDown(i);
        }
    }
    void adaugare(int x) {      ///pe ultima pozitie
        n++;    ///cresc nr de elem
        v[n] = x;
        pushUp(n);
    }
    void afisare() {
        for(int i=1; i <= n; i++) {
            cout<<v[i]<<" ";
        }
        cout<<endl;
    }
    void delHead() {    ///schimb cu ultima frunza , apoi delete
        swap(v[1],v[n]);
        n--;        ///nu sterge elem , doar scade dimensiunea vectorului
        pushDown(1);
    }
    int * Sort() {
        ///delHead pune mereu elem max pe ult poz, apoi scade dimensiunea vect ( "sterge") -> ordoneaza vectorul

        int copyn = n;    ///dimensiunea initiala a vectorului
        for(int i=1; i <= copyn; i++) {
            delHead();
        }
        n = copyn;
        return v;
    }
};

int main()
{
    int a[] = {0,20,45,100,33,73,21};
    Heap h(a,6);
    h.afisare();

    int *hSortat = h.Sort();
    for(int i=1; i <= 6; i++) {
        cout<<hSortat[i]<<" ";
    }

    return 0;
}
